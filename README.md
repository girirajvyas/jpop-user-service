# Jpop user Service  

**Dependencies used:**  


**H2 DB configurations**  


**JPARepository VS PagingAndSortingRepository VS CRUDRepository**  


**Versioning in Rest:**  
/api/v1  
/api/v2  
Advantage:  
Releasing different version with the releases help you in continous delivery  
when you have a large client base and you want to give them the liberty to upgrade as they want  

Disadvantage:  
you have to duplicate the file and make the changes, this causes code duplication  

**API request validation**  
This can be achieved with 'validation-api' and use all the respective (@NotNull, @Past, @Range)  
on the fields specified on Entity.  
To enforce this validation you have to use @Valid annotation in the Request parameter  
where it is passed. 

**Lombok**  
1. You specify dependency in pom  
   Note: You need not specify version until and unless you have to override.  
   If you hover on dependency you can see the below message:  
   "The managed version is 1.18.8 The artifact is managed in   
   org.springframework.boot:spring-boot-dependencies:2.1.6.RELEASE"  
2. Go to  "C:\Users\<user name>\.m2\repository\org\projectlombok\lombok\1.18.8"
   open command prompt here  
   command: java -jar lombok-1.18.8.jar  
   pop up -> specify location -> Install/Update  
   done -> restart STS/Eclipse  
3. use annotations  


**Advantage:**  
clients can switch version easily according to their will  

**Disadvantage:**  
code duplication for each version  

**References:**  
Code formatting  
https://github.com/HPI-Information-Systems/Metanome/wiki/Installing-the-google-styleguide-settings-in-intellij-and-eclipse  
  
Configure in memory DB  
https://www.baeldung.com/spring-boot-h2-database   
url for h2 console: http://localhost:8080/h2-console/
use datasource.url=jdbc:h2:mem:testdb and password as password configured in application.properties  


Configure Swagger      
https://www.baeldung.com/swagger-2-documentation-for-spring-rest-api  
https://dzone.com/articles/spring-boot-2-restful-api-documentation-with-swagg  
Swagger Bug: https://github.com/springfox/springfox/issues/2265#issuecomment-413286451  

AOP  
https://o7planning.org/en/10257/java-aspect-oriented-programming-tutorial-with-aspectj  

Dozer/modelMapping for DTO to Entity (vice-versa) conversions  
https://www.baeldung.com/dozer  
https://www.baeldung.com/entity-to-and-from-dto-for-a-java-spring-application   
https://www.baeldung.com/java-performance-mapping-frameworks  

Bean Validation  
https://www.mkyong.com/spring-boot/spring-rest-validation-example/  
https://www.springboottutorial.com/spring-boot-validation-for-rest-services  

plural names in API  
https://stackoverflow.com/questions/6845772/rest-uri-convention-singular-or-plural-name-of-resource-while-creating-it  

Clean code:   
https://refactoring.com/catalog/decomposeConditional.html  
http://www.kamilgrzybek.com/clean-code/10-common-broken-clean-code-rules/  

Testing  
https://spring.io/guides/gs/testing-web/  
https://www.baeldung.com/spring-boot-testing  
https://stackoverflow.com/questions/39865596/difference-between-using-mockmvc-with-springboottest-and-using-webmvctest  

Liquibase  
https://javadeveloperzone.com/spring-boot/spring-boot-liquibase-example/  
https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html#howto-execute-liquibase-database-migrations-on-startup  
https://medium.com/@harittweets/evolving-your-database-using-spring-boot-and-liquibase-844fcd7931da  
https://www.baeldung.com/liquibase-refactor-schema-of-java-app  

Note: In case you have liquibase along with h2 database, make sure you delete data.sql as it is run after the liquibase to avoid any confusion in data  

