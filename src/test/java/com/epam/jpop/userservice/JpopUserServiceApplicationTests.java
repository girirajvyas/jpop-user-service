package com.epam.jpop.userservice;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.epam.jpop.userservice.controller.UsersController;


/**
 * This test will initialize the container and verify if the beans are properly initialized <br/>
 * 
 * <pre>
 * 1. package of this test class and that of class with main method must be same, else you will get
 * IllegalStateException
 * 2. To solve this issue you can do below solutions:
 *    a. Make package declaration same.
 *    b. Provide the Application class in classes argument of @SpringBootTest.
 * </pre>
 * 
 * @author giri
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JpopUserServiceApplication.class)
public class JpopUserServiceApplicationTests {

  @Autowired
  private UsersController userServiceController;

  @Test
  public void contextLoads() {
    assertThat(userServiceController).isNotNull();
  }

}
