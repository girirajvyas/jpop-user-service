package com.epam.jpop.userservice;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import com.epam.jpop.userservice.model.UserDto;

/**
 * Server starts up on random port and test is done via TestRestTemplate
 * 
 * @author giri
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

  /**
   * used in conjunction with RANDOM_PORT to get the port on which the application is running
   */
  @LocalServerPort
  private int randomServerPort;

  @Autowired
  private TestRestTemplate testRestTemplate;

  /**
   * Check if the data that is inserted into H2 DB from data.sql can be fetched and not null
   */
  @Test
  public void testUserNotNull() {
    UserDto userDto = this.testRestTemplate
        .getForObject("http://localhost:" + randomServerPort + "/v1/users/1", UserDto.class);
    assertNotNull(userDto);
    System.out.println(userDto);
  }

}
