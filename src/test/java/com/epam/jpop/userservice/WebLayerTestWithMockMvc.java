package com.epam.jpop.userservice;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.epam.jpop.userservice.model.User;
import com.epam.jpop.userservice.model.UserDto;
import com.epam.jpop.userservice.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class WebLayerTestWithMockMvc {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UserService userService;

  @MockBean
  private ModelMapper modelMapper;

  @Test
  public void shouldReturnUserOne() throws Exception {
    // define objects to be mocked
    User user = new User((long) 1, "Giriraj", "vyas", "giriraj.vyas@gmail.com", "8087305215");
    UserDto userDto =
        new UserDto((long) 1, "Giriraj", "vyas", "giriraj.vyas@gmail.com", "8087305215");

    // mock
    when(userService.findById((long) 1)).thenReturn(Optional.of(user));
    when(modelMapper.map(user, UserDto.class)).thenReturn(userDto);

    // action
    this.mockMvc.perform(get("/api/v1/users/1")).andDo(print()).andExpect(status().isOk());
  }


}
