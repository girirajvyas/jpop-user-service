package com.epam.jpop.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.epam.jpop.userservice.model.User;

/**
 * 
 * @author giri
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
