package com.epam.jpop.userservice.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author giri
 *
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User implements Serializable {

  /**
   * Default generated serial id
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String firstName;
  private String lastName;
  private String emailId;
  private String mobileNumber;

}
