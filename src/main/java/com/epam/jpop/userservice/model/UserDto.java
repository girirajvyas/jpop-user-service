package com.epam.jpop.userservice.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author giri
 *
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "All details about the User. ")
public class UserDto {

  @ApiModelProperty(notes = "The database generated ID")
  private Long id;

  @NotNull(message = "first name must not be null")
  @Size(min = 3, max = 100, message = "first name should be minimum 3 character and max 100")
  @ApiModelProperty(notes = "user first name")
  private String firstName;

  @NotNull(message = "last name must not be null")
  @Size(min = 3, max = 100, message = "last name should be minimum 3 character and max 100")
  @ApiModelProperty(notes = "user last name")
  private String lastName;

  @NotNull(message = "Email must not be null")
  @Email(message = "Email input is invalid")
  @ApiModelProperty(notes = "Email id of the user")
  private String emailId;

  @NotNull(message = "mobile number must not be null")
  @Size(min = 10, max = 10, message = "mobile number should be 10 digits")
  @ApiModelProperty(notes = "Mobile number of the user")
  private String mobileNumber;

}
