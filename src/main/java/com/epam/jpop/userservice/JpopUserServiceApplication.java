package com.epam.jpop.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Starting point of Application
 * 
 * @author giri
 *
 */
@EnableEurekaClient
@SpringBootApplication
public class JpopUserServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(JpopUserServiceApplication.class, args);
  }

}
