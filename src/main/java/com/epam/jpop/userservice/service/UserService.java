package com.epam.jpop.userservice.service;

import java.util.List;
import java.util.Optional;
import com.epam.jpop.userservice.model.User;

/**
 * Service to fetch data from repository
 * 
 * @author giri
 *
 */
public interface UserService {

  public List<User> findAll();

  public Optional<User> findById(Long id);

  public User save(User user);

  public void deletebyId(Long id);

}
