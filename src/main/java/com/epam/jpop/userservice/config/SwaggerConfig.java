package com.epam.jpop.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Basic swagger configurations
 * <ol>
 * <li>Default: http://localhost:8080/v2/api-docs</li>
 * <li>Swagger-ui dependency: http://localhost:8080/swagger-ui.html</li>
 * </ol>
 * 
 * @author giri
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2) // docType swagger 2
        .select() // builder instantiation
        .apis(RequestHandlerSelectors.basePackage("com.epam.jpop.userservice.controller")) // package
                                                                                           // can be
                                                                                           // defined
                                                                                           // here
        .paths(PathSelectors.any()) // end-point path can be defined here
        .build().apiInfo(apiEndPointsInfo());
  }

  private ApiInfo apiEndPointsInfo() {
    return new ApiInfoBuilder().title("Users REST API").description("")
        .contact(new Contact("Giriraj Vyas", "girirajvyas.github.io", "girirajvyas21@gmail.com"))
        .license("GNU public license").version("0.0.1-SNAPSHOT").build();
  }
}
