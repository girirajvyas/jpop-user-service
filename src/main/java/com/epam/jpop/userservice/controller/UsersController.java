package com.epam.jpop.userservice.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.epam.jpop.userservice.model.User;
import com.epam.jpop.userservice.model.UserDto;
import com.epam.jpop.userservice.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Users end-point having all the end-points related to the users
 * 
 * @author giri
 *
 */
@Api(value = "User Service")
@RestController
@RequestMapping("v1/users")
public class UsersController {

  @Autowired
  private UserService userService;

  @Autowired
  private ModelMapper modelMapper;

  @ApiOperation(value = "Find all the users", response = List.class)
  @GetMapping()
  public List<UserDto> findAll() {
    List<User> users = userService.findAll();
    return users.stream().map(user -> convertToDto(user)).collect(Collectors.toList());
  }

  @ApiOperation(value = "Get user by id", response = User.class)
  @GetMapping("/{user_id}")
  public ResponseEntity<UserDto> findById(
      @ApiParam(value = "user id by which user will be retrieved",
          required = true) @PathVariable("user_id") long id) {
    Optional<User> user = userService.findById(id);
    if (!user.isPresent()) {
      return ResponseEntity.badRequest().build();
    }

    return ResponseEntity.ok(convertToDto(user.get()));
  }

  @ApiOperation(value = "Add a user", response = User.class)
  @PostMapping
  public ResponseEntity<UserDto> create(@ApiParam(value = "User object store in database table",
      required = true) @Valid @RequestBody UserDto userDto) {
    return ResponseEntity.status(HttpStatus.CREATED)
        .body(convertToDto(userService.save(convertToEntity(userDto))));
  }

  @ApiOperation(value = "update a user", response = User.class)
  @PutMapping("/{user_id}")
  public ResponseEntity<UserDto> update(
      @ApiParam(value = "user id by which user will be retrieved",
          required = true) @PathVariable("user_id") long id,
      @ApiParam(value = "User object store in database table",
          required = true) @Valid @RequestBody UserDto userDto) {
    if (userService.findById(id).isPresent()) {
      ResponseEntity.badRequest().build();
    }
    return ResponseEntity.ok().body(convertToDto(userService.save(convertToEntity(userDto))));
  }

  @ApiOperation(value = "delete a user", response = User.class)
  @DeleteMapping("/{user_id}")
  public ResponseEntity<User> delete(@ApiParam(value = "user id by which user will be retrieved",
      required = true) @PathVariable("user_id") long id) {
    if (userService.findById(id).isPresent()) {
      ResponseEntity.badRequest().build();
    }
    userService.deletebyId(id);
    return ResponseEntity.ok().build();
  }

  private User convertToEntity(UserDto userDto) {
    return modelMapper.map(userDto, User.class);
  }

  private UserDto convertToDto(User user) {
    return modelMapper.map(user, UserDto.class);
  }
}
