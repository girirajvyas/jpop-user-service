DROP TABLE IF EXISTS user;
 
CREATE TABLE user (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_Name VARCHAR(100) NOT NULL,
  last_Name VARCHAR(100) NOT NULL,
  email_Id VARCHAR(50) NOT NULL,
  mobile_Number VARCHAR(10) NOT NULL,
);
 
INSERT INTO user (first_Name, last_Name, email_Id, mobile_Number) VALUES
  ('Giriraj2', 'Vyas2', '2giriraj_vyas@epam.com', '2087305215'),
  ('Yuvraj2', 'Singh2', '2yuvraj_singh@epam.com', '2888888888');